/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

/**
 *
 * @author joenice
 */
public interface SearchTree {
    public void Insert(int n);//Add a node the tree with value n.

    public boolean Find(int n); //Find a node the tree with value n. Its O(lgn)

    public void Delete(int n);// Delete a node the tree with value n. Its O(lgn)

    public void Display();//Prints the entire tree in increas­ing order. O(n).
}

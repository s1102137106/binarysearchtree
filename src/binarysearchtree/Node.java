/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

/**
 *
 * @author joenice
 */
public class Node {

    public int data;
    public Node left;
    public Node right;
    public Node parent;//雙向鏈結

    public Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }

    public void ReInsertNode(int inputData) {
        if (inputData > this.data) {//放到右邊
            if (this.right != null) {//往右繼續尋找
                this.right.ReInsertNode(inputData);
            } else {
                this.right = new Node(inputData);
            }
        } else {//放到左邊
            if (this.left != null) {//往左繼續尋找
                this.left.ReInsertNode(inputData);
            } else {
                this.left = new Node(inputData);
            }
        }
    }

    public Node ReFindNode(int inputData) {
        if (inputData == this.data) {
            return this;
        }
        if (inputData > this.data) {
            if (this.right != null) {//往右繼續尋找
                return this.right.ReFindNode(inputData);
            }
        } else {
            if (this.left != null) {//往左繼續尋找
                return this.left.ReFindNode(inputData);
            }
        }
        return null;
    }

    //中序輸出即可得出由小到大
    public void Display() {
        if (this.left != null) {
            this.left.Display();
        }
        System.out.printf("%d ", this.data);
        if (this.right != null) {
            this.right.Display();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

/**
 *
 * @author joenice
 */
public class BinarySearchTree implements SearchTree {

    protected Node root;

    @Override
    public void Insert(int n) {
        if (root == null) {
            root = new Node(n);
            return;
        }
        root.ReInsertNode(n);
    }

    @Override
    public boolean Find(int n) {
        return root.ReFindNode(n) != null;
    }

    @Override
    public void Delete(int n) {
        Node parent = root;
        Node current = root;//要刪除的節點
        boolean isLeftChild = false;
        while (current.data != n) {
            parent = current;
            if (current.data > n) {
                isLeftChild = true;
                current = current.left;
            } else {
                isLeftChild = false;
                current = current.right;
            }
            if (current == null) {
                System.out.printf("%d is not fuond!", n);
                System.out.println("");
                return;
            }
        }

        //if i am here that means we have found the node
        //Case 1: if node to be deleted has no children
        if (current.left == null && current.right == null) {
            if (current == root) {
                root = null;
            }
            if (isLeftChild == true) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } //Case 2 : if node to be deleted has only one child
        else if (current.right == null) {
            SetCurrnetNode(current.left, parent, isLeftChild);
        } else if (current.left == null) {
            SetCurrnetNode(current.right, parent, isLeftChild);
        } else if (current.left != null && current.right != null) {
            Node successor = getSuccessor(current);
            SetCurrnetNode(successor, parent, isLeftChild);
            successor.left = current.left;
        }

    }

    public Node getSuccessor(Node deleleNode) {
        Node successsor = null;
        Node successsorParent = null;
        Node current = deleleNode.right;
        while (current != null) {
            successsorParent = successsor;
            successsor = current;
            current = current.left;
        }
        //check if successor has the right child, it cannot have left child for sure
        // if it does have the right child, add it to the left of successorParent.
        //successsorParent
        if (successsor != deleleNode.right) {
            successsorParent.left = successsor.right;
            successsor.right = deleleNode.right;
        }
        return successsor;
    }

    public void SetCurrnetNode(Node currentChild, Node parent, boolean isLeftChild) {
        if (currentChild == root) {
            root = currentChild;
        } else if (isLeftChild) {
            parent.left = currentChild;
        } else {
            parent.right = currentChild;
        }
    }

    @Override
    public void Display() {
        this.root.Display();
    }

}

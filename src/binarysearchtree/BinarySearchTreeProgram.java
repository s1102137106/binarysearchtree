/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

/**
 *
 * @author joenice
 */
public class BinarySearchTreeProgram {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BinarySearchTree tree = new AVLTree();

        int[] testData = {5, 3, 1, 6, 7, 2, 4};

        for (int item : testData) {
            tree.Insert(item);
        }
//        tree.Delete(4);
//        tree.Delete(9);
        tree.Display();

        //System.out.println(tree.Find(4));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

/**
 *
 * @author joenice
 */
public class AVLNode extends Node {

    private int leftHight = 0;
    private int rightHight = 0;
    private boolean parentLeftChild;

    static Balance blanceStatus = Balance.NOTHING;

    //LEFT , RIGHT 代表第一層 
    private enum Balance {
        NOTHING, LEFT, RIGHT, LEFT_LEFT, LEFT_RIGHT, RIGHT_LEFT, RIGHT_RIGHT
    };

    public AVLNode(int data, boolean parentLeftChild) {
        super(data);
        this.parentLeftChild = parentLeftChild;
    }

    @Override
    public void ReInsertNode(int inputData) {

        if (inputData > this.data) {//放到右邊

            if (this.right != null) {//往右繼續尋找
                this.right.ReInsertNode(inputData);
            } else {
                this.right = new AVLNode(inputData, false);
                this.right.parent = this;
                AddHight();
                if (!IsImblance()) {

                }
            }
        } else {//放到左邊
            if (this.left != null) {//往左繼續尋找
                this.left.ReInsertNode(inputData);
            } else {
                this.left = new AVLNode(inputData, true);
                this.left.parent = this;
                AddHight();
                if (!IsImblance()) {

                }

            }
        }
    }

    private void AddHight() {
        if (this.parent == null) {//root的情況
            return;
        }
        if (this.parentLeftChild) {
            ((AVLNode) this.parent).leftHight++;
        } else {
            ((AVLNode) this.parent).rightHight++;
        }
         ((AVLNode) this.parent).AddHight();//往上繼續加
    }

    private boolean IsImblance() {
        // System.out.print(Math.abs(this.rightHight - this.leftHight));
        return Math.abs(this.rightHight - this.leftHight) < 2;
    }

    private void ConvertToAvlTree() {

    }

}
